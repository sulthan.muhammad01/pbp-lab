### 1. Apakah perbedaan antara JSON dan XML?

| JSON                            | XML                                                          |
| :------------------------------ | :----------------------------------------------------------- |
| *JavaScript Object Nation*      | *eXtensible Markup Language*                                 |
| Memiliki basis JavaScripts      | Memiliki basis turunan SGML (*standard generalized markup language*) |
| Tidak bisa menambahkan komentar | Bisa menambahkan komentar                                    |
| Bisa menggunakan Array          | Tidak bisa menggunakan Array                                 |
| Representasi data berupa *map*  | Representasi data berupa *tree*                              |

### 2. Apakah berbedaan HTML dan XML

| HTML                                       | XML                                  |
| ------------------------------------------ | ------------------------------------ |
| *HyperText Markup Language*                | *eXtensible Markup Language*         |
| *Static* (tidak dapat dilakukan perubahan) | *Dynamic* (bisa dilakukan perubahan) |
| Tidak *case sensitive*                     | *Case sensitive*                     |
| Berfungsi untuk menyajikan data            | Berfungsi untuk mentransfer data     |
| Tidak membutuhkan *closing tags*           | Membutuhkan *closing tags*           |

Sumber:

https://www.educba.com/json-vs-xml/ 

https://www.guru99.com/json-vs-xml-difference.html

https://www.geeksforgeeks.org/difference-between-json-and-xml/

https://www.geeksforgeeks.org/html-vs-xml/