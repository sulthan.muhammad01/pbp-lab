import 'dart:ui';

import 'package:flutter/material.dart';
import '../constants.dart';

class UserScreen extends StatelessWidget {
  final String name;
  final String urlImage;

  const UserScreen({
    Key? key,
    required this.name,
    required this.urlImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    extendBodyBehindAppBar: true,
    appBar: AppBar(
      backgroundColor: Colors.transparent,
      title: Text(name, style: TextStyle(color: kPrimaryColor),),
      centerTitle: true,
      elevation: 0,
    ),
    body: 
    Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(urlImage),
          fit: BoxFit.cover
        )
      ),
      child:
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Center(
              child: Text(""),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: 
            Container(
              margin: EdgeInsets.only(bottom: 40, left: 105, right: 105),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.black.withOpacity(0.05)
              ),
              child: ElevatedButton(
                style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                  ),
                  minimumSize: MaterialStateProperty.all(Size(50, 50)),
                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                  shadowColor: MaterialStateProperty.all(Colors.transparent),
                ),
                child: Container(
                  padding: const EdgeInsets.only(top: 4, bottom: 4),
                  child: 
                  RichText(
                    text: TextSpan(
                      style: Theme.of(context).textTheme.bodyText1,
                      children: [
                        WidgetSpan(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children:[
                              Icon(Icons.keyboard_arrow_up_outlined, color: kPrimaryColor,),
                              Text("Click for details!",),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                onPressed: () {
                  showModalBottomSheet(
                  context: context, 
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30)
                      ),
                  ),
                  backgroundColor: kBackgroundColor.withOpacity(0.9),
                  builder: ((builder) => 
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.black.withOpacity(0.05),
                      ),
                      child: 
                      Wrap(
                        children:[
                        Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(height: 30,),
                          RichText(
                            text: TextSpan(
                              children: [
                                WidgetSpan(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children:[
                                      Icon(Icons.person, color: kPrimaryColor,),
                                      SizedBox(width: 10,),
                                      Text("@username",
                                        style: TextStyle(color: kTextColor, fontWeight: FontWeight.w600),),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 20,),
                          RichText(
                            text: TextSpan(
                              children: [
                                WidgetSpan(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children:[
                                      Icon(Icons.phone, color: kPrimaryColor,),
                                      SizedBox(width: 10,),
                                      Text("+62XXXXXXXX",
                                        style: TextStyle(color: kTextColor, fontWeight: FontWeight.w600),),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 20,),
                          RichText(
                            text: TextSpan(
                              children: [
                                WidgetSpan(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children:[
                                      Icon(Icons.email, color: kPrimaryColor,),
                                      SizedBox(width: 10,),
                                      Text("joe@mama.com",
                                        style: TextStyle(color: kTextColor, fontWeight: FontWeight.w600),),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 20,),
                          RichText(
                            text: TextSpan(
                              children: [
                                WidgetSpan(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children:[
                                      Icon(Icons.person_pin_circle_rounded, color: kPrimaryColor,),
                                      SizedBox(width: 10,),
                                      Text("Beji, Depok, Jawa Barat",
                                        style: TextStyle(color: kTextColor, fontWeight: FontWeight.w600),),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 30,),
                        ],
                      ),
                        ],
                      ),
                    )),
                  );
                },
              ),
            ),    
          ),
        ],
      )
    ),
  );
}