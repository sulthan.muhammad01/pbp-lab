import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import '../constants.dart';
import '../config/styles.dart';
import '../widgets/widgets.dart';
import '../screens/screens.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' hide Style;

class ProfileSettings extends  StatefulWidget{
  @override
  State<StatefulWidget> createState() {
     return _ProfileSettings();
  }
}

class _ProfileSettings extends State<ProfileSettings>{

  final _formKey = GlobalKey<FormState>();
  bool checkedValue = false;
  bool checkboxValue = false;
  File? image;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(headerSliverBuilder: (context, innerBoxIsScrolled) => [
      SliverAppBar(
        title: Text("Profile", 
          style: TextStyle(fontWeight: FontWeight.w600,
          color: kPrimaryColor
          ),
        ),
        iconTheme: IconThemeData(color: kPrimaryColor),
        elevation: 0,
        //centerTitle: true,
        backgroundColor: kBackgroundColor,
        ),
      ],
      body: SingleChildScrollView(
        child: 
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: kDefaultPadding * 2, top: kDefaultPadding *1.3),
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 35),
                child: 
                Center(
                  child: 
                    Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              spreadRadius: 2,
                              blurRadius: 10,
                              color: Colors.black.withOpacity(0.1),
                              offset: const Offset(0, 10)
                            )
                          ],
                        ),
                        child: image != null 
                        ? CircleAvatar(
                          radius: 80,
                          backgroundImage: FileImage(image!),
                        )
                        : CircleAvatar(
                          radius: 80,
                          backgroundImage: NetworkImage(
                            'https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=687&q=80',
                          ),
                        )
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: 4,
                              color: kPrimaryColor2,
                            ),
                            color: kPrimaryColor2,
                          ),
                          child: InkWell (
                            onTap: () {showModalBottomSheet(
                              context: context, 
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30),
                                  topRight: Radius.circular(30)
                                  ),
                              ),
                              backgroundColor: kBackgroundColor.withOpacity(0.9),
                              builder: ((builder) => NewBottomSheet()),
                              );
                            },
                            child: Icon(
                              Icons.edit,
                              color: Colors.white,
                              )
                          )
                        )
                      ),
                    ],
                  ),
                ),
              ),
              Container (
                margin: EdgeInsets.fromLTRB(25, 0, 25, 10),
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration('Nama Depan', 'Joe'),
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Mohon masukkan nama depan anda";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 30,),
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration('Nama Belakang', 'Mama'),
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Mohon masukkan nama belakang anda";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration("Username", "@username"),
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Mohon masukkan username anda";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration("Alamat E-mail", "admin@example.com"),
                              keyboardType: TextInputType.emailAddress,
                              validator: (val) {
                                if(!(val!.isEmpty) && !RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$").hasMatch(val)){
                                  return "Masukkan alamat email yang valid!";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration("Jenis Kelamin", "Pria/Wanita"),
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Mohon isi jenis kelamin anda";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration('Tanggal Lahir', '1/1/1111'),
                              keyboardType: TextInputType.datetime,
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Mohon masukkan tanggal lahir anda";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration(
                                  "No Telp.",
                                  "Masukkan no telp. anda"),
                              keyboardType: TextInputType.phone,
                              validator: (val) {
                                if(!(val!.isEmpty) && !RegExp(r"^(\d+)*$").hasMatch(val)){
                                  return "Masukkan no telp. yang valid!";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration("Provinsi", "Jawa Barat"),
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Mohon masukkan nama provinsi anda";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration("Kota/Kabupaten", "Depok"),
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Mohon masukkan nama kota/kabupaten anda";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextFormField(
                              decoration: Style().textInputDecoration("Kecamatan", "Beji"),
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return "Mohon masukkan nama kecamatan anda";
                                }
                                return null;
                              },
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 20,),
                          Container(
                            child: TextFormField(
                              minLines: 3,
                              maxLines: 8,
                              decoration: Style().textInputDecoration("Bio", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae arcu in odio maximus laoreet. In venenatis velit ut augue semper, nec aliquet sapien vulputate. Praesent mollis feugiat justo."),
                            ),
                            decoration: Style().inputBoxDecorationShaddow(),
                          ),
                          SizedBox(height: 30.0),
                          Container(
                            decoration: Style().buttonBoxDecoration(context),
                            child: ElevatedButton(
                              style: Style().buttonStyle(),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                                child: Text(
                                  "Update Profil",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) => HomeScreen()
                                      ),
                                          (Route<dynamic> route) => false
                                  );
                                }
                              },
                            ),
                          ),               
                          SizedBox(height: 30.0),
                          Divider(color: kPrimaryColor),
                          SizedBox(height: 30.0,),
                          Container(
                            decoration: Style().deactivateButtonBoxDecoration(context),
                            child: ElevatedButton(
                              style: Style().buttonStyle(),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                                child: Text(
                                  "Deactivate",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              onPressed: () {
                                showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30.0))),
                                    scrollable: true,
                                    title: Center(child: Text('Hapus Akun?'),),
                                    content: Text("Apakah anda yakin ingin menghapus akun ini?"),
                                    actions: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [ 
                                          ElevatedButton(
                                            child: Text("Tidak"),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            style: ButtonStyle(
                                              backgroundColor: MaterialStateProperty.resolveWith<Color>(
                                                (Set<MaterialState> states) {
                                                  if (states.contains(MaterialState.pressed))
                                                    return Colors.red.shade100;
                                                  return Colors.red;
                                                  },
                                                ),
                                              ),
                                            ),
                                          SizedBox(width: 20,),
                                          ElevatedButton(
                                            child: Text("Yakin"),
                                            onPressed: () {
                                            },
                                            style: ButtonStyle(
                                              backgroundColor: MaterialStateProperty.resolveWith<Color>(
                                                (Set<MaterialState> states) {
                                                  if (states.contains(MaterialState.pressed))
                                                    return kTextLightColor;
                                                  return kPrimaryColor2;
                                                  },
                                                ),
                                              ),
                                            ),
                                          ], 
                                        ),
                                      ],
                                    );
                                  }
                                );
                              },
                            ),
                          ),
                          SizedBox(height: 30.0),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget NewBottomSheet() {
    return Container(
      child: Wrap(
        children: [ 
          Column(      
            children: <Widget> [
              SizedBox(height: 20,),
              Text(
                "Pilih foto profil",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 20),
              ListTile(
                leading: Icon(Icons.camera, color: kPrimaryColor,),
                onTap: (){
                  pickImage(ImageSource.camera);
                }, 
                title: Text("Kamera"),
              ),
              ListTile(
                leading: Icon(Icons.image_outlined, color: kPrimaryColor,),
                onTap: (){
                  pickImage(ImageSource.gallery);
                }, 
                title: Text("Galeri"),
              ),
              SizedBox(height: 20),
            ],
          ),
        ],
      ),
    );
  }

  Future pickImage(ImageSource source) async{
    try{
      final image = await ImagePicker().pickImage(
        source: source
      );
      if (image==null) return;

      final imagePermanent = await saveImagePermanently(image.path);
      setState(() {
        this.image = imagePermanent;
      });
    } on PlatformException catch (e) {
      print('Gagal mengambil gambar: $e');
    }
  }

  Future <File> saveImagePermanently(String imagePath) async{
    final directory = await getApplicationDocumentsDirectory();
    final name = basename(imagePath);
    final image = File('${directory.path}/$name');

    return File(imagePath).copy(imagePath);
  }
}