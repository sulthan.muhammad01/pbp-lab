import 'package:flutter/material.dart';
import '../config/styles.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../screens/screens.dart';

class ChangePassword extends  StatefulWidget{
  @override
  State<StatefulWidget> createState() {
     return _ChangePassword();
  }
}

class _ChangePassword extends State<ChangePassword>{

  final _formKey = GlobalKey<FormState>();
  bool checkedValue = false;
  bool checkboxValue = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*drawer: Container(
        width: MediaQuery.of(context).size.width * 0.74,
        child: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
          child: NavigationDrawerWidget(),
        ),
      ),*/
      appBar: AppBar(
        title: Text("Password", 
          style: TextStyle(fontWeight: FontWeight.w600,
          color: kPrimaryColor
          ),
        ),
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child:
        Container (
          margin: EdgeInsets.fromLTRB(25, 35, 25, 10),
          alignment: Alignment.center,
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                      child: TextFormField(
                        obscureText: true,
                        decoration: Style().textInputDecoration(
                            "Password Lama", "*********"),
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Mohon masukkan password lama anda";
                          }
                          return null;
                        },
                      ),
                      decoration: Style().inputBoxDecorationShaddow(),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: TextFormField(
                        obscureText: true,
                        decoration: Style().textInputDecoration(
                            "Ulangi Password Lama", "*********"),
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Mohon masukkan password lama anda";
                          }
                          return null;
                        },
                      ),
                      decoration: Style().inputBoxDecorationShaddow(),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: TextFormField(
                        obscureText: true,
                        decoration: Style().textInputDecoration(
                            "Password Baru", "*********"),
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Mohon masukkan password baru anda";
                          }
                          return null;
                        },
                      ),
                      decoration: Style().inputBoxDecorationShaddow(),
                    ),
                    SizedBox(height: 30.0),
                    Container(
                      decoration: Style().buttonBoxDecoration(context),
                      child: ElevatedButton(
                        style: Style().buttonStyle(),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                          child: Text(
                            "Ganti Password",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => HomeScreen()
                                ),
                                    (Route<dynamic> route) => false
                            );
                          }
                        },
                      ),
                    ),
                    SizedBox(height: 30.0),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}