import '../constants.dart';
import '../widgets/widgets.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Container(
        width: MediaQuery.of(context).size.width * 0.74,
        child: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
          child: NavigationDrawerWidget(),
        ),
      ),
      //body: NestedScrollView(headerSliverBuilder: (context, innerBoxIsScrolled) => [
      appBar: AppBar(
        title: Text("COVID-19", style: TextStyle(color: kPrimaryColor, fontWeight: FontWeight.w600),),
        centerTitle: true,
        backgroundColor: kPrimaryColor.withOpacity(0.03),
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.help),
            tooltip: "Punya Pertanyaan",
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    scrollable: true,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30.0))),
                    title: Center(child: Text('Punya pertanyaan?')),
                    content: Form(
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            minLines: 1, 
                            maxLines: 8,
                          ),
                        ],
                      ),
                    ),
                    actions: [
                      Center(
                        child:
                        ElevatedButton(
                          child: Text("Kirim"),
                          onPressed: () {
                          },
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) {
                                if (states.contains(MaterialState.pressed))
                                  return kTextLightColor;
                                return kPrimaryColor2;
                                },
                              ),
                            ),
                          )
                      )
                      ],
                    );
                  }
                );
              }
            ),
          ],
        ),
      //],

//wrap singlechildscrollview for correct displaying in small density devices
      body: SingleChildScrollView(
        child: 
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                color: kPrimaryColor.withOpacity(0.03),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(50),
                  bottomRight: Radius.circular(50),
                ),
              ),
              child: Wrap(
                runSpacing: 20,
                spacing: 20,
                children: <Widget>[
                  InfoCard(
                    title: "TERKONFIRMASI",
                    icon: Icons.warning,
                    iconColor: Color(0xFFFF8C00),
                    effectedNum: 1062,
                  ),
                  InfoCard(
                    title: "DALAM PERAWATAN",
                    icon: Icons.local_hospital,
                    iconColor: Color(0xFF50A3C2),
                    effectedNum: 75,
                  ),
                  InfoCard(
                    title: "SEMBUH",
                    icon: Icons.check_circle,
                    iconColor: Color(0xFF50E3C2),
                    effectedNum: 689,
                  ),
                  InfoCard(
                    title: "MENINGGAL",
                    icon: Icons.airline_seat_flat_rounded,
                    iconColor: Color(0xFFFF2D55),
                    effectedNum: 75,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: WeeklyChart()
            )
          ],
        ),
      ),
    );
  }
}