# Generated by Django 3.2.7 on 2021-09-19 12:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='friend',
            old_name='date_of_birt',
            new_name='date_of_birth',
        ),
    ]
